# NovelReader

## 当前版本1.0
本项目是利用Nodejs、NW实现的小说阅读应用，现在支持从www.64mi.com下载小说。只是为学习Nodejs来练手的，页面部分采用了Extjs3.4gpl，主要本人对前端的CSS不熟悉，为了快速搭建页面只能采用成熟的组件库。

## 使用方法

###nw
- 修改package.json中的“main”为“Reader.html”;
- 执行npm install，在app目录下，执行"npm install"；
- 执行nw，要求得先安装了nw，执行"npm install -g nw"即可；
- 点击“添加小说”按钮，将要下载的小说章节列表页面的地址录入，确认后会下载。
- 小说阅读时，可以利用快捷键。
   + j:前一章
   + k:后一章
   + h:回到目录

###electron
- 修改package.json中的“main”为“app.js”;
- 执行npm install，在app目录下，执行"npm install"；
- 执行electro，要求得先安装了electro，执行"npm install -g electron-prebuilt"即可；

## 规划
- 增加可持续更新小说的功能
- 增加下载配置，可支持更多的站点