var app = require("app");
var browser = require("browser-window");
var mainWindow = null;
app.on("window-all-closed", () => { 
    if (process.platform != "darwin") {
        app.quit();
    }
});
app.on("ready", () => { 
    mainWindow = new browser({ frame:true,width: 1000, height: 600 });
    mainWindow.loadURL("file://" + __dirname + "/Reader.html");
    mainWindow.on("closed", () => {
        mainWindow = null;
    });
});