"use strict";
var fs = require("fs");
var novel = require("./src/Novel.js");
var total = 0;
var selected = "";
Ext.ns("Reader");
Reader.ContentPanel = Ext.extend(Ext.Panel, {
    region:"center",
    id: "contentPanel",
    constructor: function () {
        var me = this;
        Reader.ContentPanel.superclass.constructor.call(this, {
            frame: false,
			layout:"border",
            items: [{
                xtype: "panel",
				region:"center",
                html: ""
            }]
        });
    },
    showContent: function (url) {
        var html = "<iframe frameborder=0 scrolling='auto' width='100%' height='100%' src='" + url + "'></iframe>";
        this.items.get(0).update(html);
    }
});
Reader.NovelList = Ext.extend(Ext.Panel, {
    autoHeight:true,
    autoWidth: true,
    border: false,

    constructor: function (config) {
        Ext.apply(this, config);
        var store = Ext.create("Ext.data.Store",{
            autoLoad:false,
            fields: ["name", "chapters"]
        });
        
        var listView =Ext.create("Ext.grid.Panel",{
            store: store,
            //multiSelect: false,
            emptyText: '无下载内容',
            hideHeaders:false,
            columns: [
                {
                    text: '名称',
                    dataIndex: 'name',
					align:"center"
                },
                {
                    text: '章节数',
                    dataIndex: 'chapters',
					align:"center"
                }
            ],
            listeners: {
                rowclick: function (listview, record,tr,index, e, opts) {
                    //var selected= listview.store.getAt(index).get("name");
                    selected=record.get("name");
                    Ext.getCmp("contentPanel").showContent("./download/" + selected + "/index.html");
                }
            }
        });
        Reader.NovelList.superclass.constructor.call(this, {
            frame: false,
            items: [
                listView
            ]
        });
    },
    refreshList: function () {
        var me = this;
        fs.readdir("./download", function (err, files) {
            if (err) {
                console.log(err);
            }
            var datas = [];
            files.map(function (element) {
                var n = novel.Novel.loadFromDirectory("./download/" + element);
                datas.push({name:n.name, chapters:n.chapters + "章"});
            });
            me.items.get(0).getStore().loadData(datas);
            console.log(me.items.get(0).getStore().getData().getAt(0));
        });
    }
});
Reader.LeftPanel = Ext.extend(Ext.Panel, {
    region:"west",
	width:window.innerWidth*0.15,
	split:true,
    layout: "border",
    constructor: function () {
        var tb = new Ext.Toolbar({
            region:"north",
            height: 50,
            buttonAlign: "center",
            items: [
                {
                    xtype: 'button',
                    text: '添加小说',
                    autoWidth: true,
                    cls: 'btn',
                    handler: function (b, e) {
						var option={
							prompt: true,
							title: "下载地址",
							minWidth: 360,
							message: "请输入下载地址:",
							buttons: Ext.Msg.OKCANCEL,
							callback: function (btn, text) {
								if (btn == "ok") {
									var n2 = new novel.Novel();
									n2.downloadFrom(text,
										function (err, novel) {
											total = novel.chapters;
											console.log("开始下载 " + novel.name + ",共" + novel.chapters + "章节");
										},
										function (count) {
											Ext.getCmp("statusBar").updateProgress(count / total, '已下载 ' + count + ' ,共 ' + total + '项');
										},
										function (err, novel) {
											//刷新列表
											Ext.getCmp("novelList").refreshList();
										});
								}
							},
							width:360
						}
						Ext.Msg.show(option);
                    }
                },
                { xtype: 'tbspacer' },
                { xtype: 'tbspacer' },
                {
                    xtype: 'button',
                    text: '删除小说',
                    cls: 'btn',
                    autoWidth: true,
                    handler: function (b, e) {
						if(selected!=undefined && selected!=""){
							Ext.Msg.confirm("删除小说", "确认删除该小说吗？", function (btn) {
								if (btn == "yes") {
									var n = new novel.Novel();
									n._name = selected;
									n.directory = "./download/"+selected;
									n.delete(function (err, msg) {
										if (err) {
											console.log(err);
										} else {
											console.log(msg);
										}
									});
									Ext.getCmp("novelList").refreshList();
									alert("删除"+selected);
								}
							});
						}else{
							alert("请选择要删除的小说！");
						}
                    }
                }
            ]
        });
        var novelList = new Reader.NovelList({
            region: "center",
            id:"novelList"
        });
        var statusBar = new Ext.ProgressBar({
            region:"south",
            text: '当前无下载',
            id: "statusBar",
            height: 25
        });
        Reader.LeftPanel.superclass.constructor.call(this, {
            frame: false,
            items: [
                tb, novelList, statusBar
            ]
        });
    },
    downloadProcess: function (v, count) {
        this.items.get(2).updateProgress(v / count, '已下载 ' + v + ' of ' + count + '...');
    },
    updateNovelList: function () {
        Ext.getCmp("novelList").refreshList();
    }
});