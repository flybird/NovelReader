var novel = require("./src/Novel.js");
var n1 = new novel.Novel();
n1._name = "完美世界";
n1._chapters = 20;
console.log(n1.name);
console.log(n1.chapters);
/** 
 * 从网络下载小说

var n2 = new novel.Novel();
var total = 0;
function beforeDownload(err, novel) {
    total = novel.chapters;
    console.log("开始下载 "+novel.name+",共"+novel.chapters+"章节");
}
function progress(err, count) {
    if (err) {
        console.log(err)
    } else {
        console.log("已下载:" + count/total);
    }
}
function afterDownload(err, novel) {
    console.log(novel.name+" 下载完成");
}
n2.downloadFrom("http://www.64mi.com/book/42/42062/", beforeDownload, progress, afterDownload);
console.log(n2.name);
*/
var n3 = novel.Novel.loadFromDirectory("./download/武炼巅峰");
console.log(`${n3.name},共${n3.chapters}`);
n3.delete(function (err, msg) {
    if (err) {
        console.log(err);
    } else {
        console.log(msg);
    }
});