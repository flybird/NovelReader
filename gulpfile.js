'use strict';
var gulp = require('gulp');
var uglify=require("gulp-uglify");
var childProcess = require('child_process');
var concat=require("gulp-concat");
var rename=require("gulp-rename");
// 创建 gulp 任务
gulp.task('uglify',function(){
    gulp.src("app/src/*.js")
    .pipe(concat("all.js"))
    .pipe(gulp.dest("app/dist/"))
    .pipe(rename("app.min.js"))
    .pipe(uglify()).on("error",function(e){
        console.log(e);
    })
    .pipe(gulp.dest("app/dist/"));
});